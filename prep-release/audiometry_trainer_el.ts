<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>SpreadsheetWidget</name>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="594"/>
        <source>Bone R mdp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="594"/>
        <source>Bone R width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="594"/>
        <source>Bone L mdp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="594"/>
        <source>Bone L width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="594"/>
        <source>Gain R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="594"/>
        <source>Gain L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="680"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="681"/>
        <source>Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="682"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="683"/>
        <source>Set value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="594"/>
        <source>ABG R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="594"/>
        <source>F. A. rate R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="594"/>
        <source>Lapse rate R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="594"/>
        <source>ABG L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="594"/>
        <source>F. A. rate L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="594"/>
        <source>Lapse rate L</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>applicationWindow</name>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1660"/>
        <source>Supra-aural</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1973"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="226"/>
        <source>Chan. 1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1660"/>
        <source>Circum-aural</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1660"/>
        <source>Insert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1978"/>
        <source>Bone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="243"/>
        <source>Test ear:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1792"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="972"/>
        <source>Lock channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="276"/>
        <source>Show response ear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="461"/>
        <source>Chan. 2:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="474"/>
        <source>Chan. 2 level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="499"/>
        <source>Chan. 2 ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="546"/>
        <source>Grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="173"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="175"/>
        <source>Exit application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="189"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="196"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="2123"/>
        <source>About audiometry_trainer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="201"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="323"/>
        <source>Show estimated thresholds:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="387"/>
        <source>Show actual thresholds:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="391"/>
        <source>Thresh. air R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="403"/>
        <source>Thresh. air L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="415"/>
        <source>Thresh. bone R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="426"/>
        <source>Thresh. bone L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="328"/>
        <source>Thresh. unm. air R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="337"/>
        <source>Thresh. unm. air L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="344"/>
        <source>Thresh. unm. bone R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="351"/>
        <source>Thresh. unm. bone L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="358"/>
        <source>Thresh. msk. air R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="372"/>
        <source>Thresh. msk. bone R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="379"/>
        <source>Thresh. msk. bone L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="888"/>
        <source>Choose case file to load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="365"/>
        <source>Thresh. msk. air L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="178"/>
        <source>Load case</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="180"/>
        <source>Load case file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="431"/>
        <source>CI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="306"/>
        <source>Mark threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="312"/>
        <source>No response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="317"/>
        <source>Masking dilemma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="491"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="197"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="968"/>
        <source>Unlock channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="101"/>
        <source>Frequency (kHz)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="102"/>
        <source>Level (dB HL)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1685"/>
        <source>Sound field</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1754"/>
        <source>Unaided</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1756"/>
        <source>Aided</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="450"/>
        <source>Non-test ear status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1723"/>
        <source>Chan. 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1697"/>
        <source>Earplug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1740"/>
        <source>Earmold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1730"/>
        <source>Dome (open)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="253"/>
        <source>Test ear status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1732"/>
        <source>Dome (tulip)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1734"/>
        <source>Dome (single vent)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1736"/>
        <source>Dome (double vent)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1738"/>
        <source>Double dome (power)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="182"/>
        <source>Load random case</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="184"/>
        <source>Load random case file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1443"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1301"/>
        <source>Requested channel 2 value out of limits for the current transducers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1315"/>
        <source>Channel 2 has reached its maximum level for the current transducers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1329"/>
        <source>Channel 2 has reached its minimum level for the current transducers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1371"/>
        <source>Channel 1 has reached the minimum level for the current transducers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1377"/>
        <source>Channel 2 is locked to channel 1 and has reached its minimum level for the current transducers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1388"/>
        <source>Channel 1 has reached the maximum level for the current transducers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1394"/>
        <source>Channel 2 is locked to channel 1 and has reached its maximum level for the current transducers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="186"/>
        <source>Generate case</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="264"/>
        <source>Test ear coupling:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="296"/>
        <source>Response light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="514"/>
        <source>Show case filename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="521"/>
        <source>Show case info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="529"/>
        <source>Case file: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="560"/>
        <source>Stimulus light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="563"/>
        <source>Play stimulus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="885"/>
        <source>No case info available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="888"/>
        <source>CSV files (*.csv *CSV *Csv);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="290"/>
        <source>Auto. thresh. search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="282"/>
        <source>Show response counts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1443"/>
        <source>Automatic threshold search does not work when channel 2 is ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="203"/>
        <source>Manual (html)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="207"/>
        <source>Manual (pdf)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1647"/>
        <source>Uncovered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="1692"/>
        <source>Earphone on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="220"/>
        <source>?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="238"/>
        <source>Select the transducer for the test ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="249"/>
        <source>Select the test ear (right or left).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="258"/>
        <source>Set the status (aided or unaided) of the test ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="269"/>
        <source>Select coupling option for the test ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="510"/>
        <source>Lock the channels so that changing the stimulus level automatically changes the masking noise level by the same amount.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="293"/>
        <source>Perform an automatic threshold search using the Hughson-Westlake procedure.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="280"/>
        <source>Response light will turn red/blue if the right/left ear is responding. White if both ears respond.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="286"/>
        <source>Show the counts of responses on ascending level trials.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="303"/>
        <source>The light will turn on if the virtual listener responds to the stimulus.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="308"/>
        <source>Mark the threshold at the current level.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="314"/>
        <source>Mark in the audiogram that no response was obtained at the highest stimulus level.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="319"/>
        <source>Mark in the audiogram that the current threshold cannot be established because of a masking dilemma.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="332"/>
        <source>Show the unmasked air conduction thresholds that you&apos;ve estimated for the right ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="341"/>
        <source>Show the unmasked air conduction thresholds that you&apos;ve estimated for the left ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="348"/>
        <source>Show the unmasked bone conduction thresholds that you&apos;ve estimated for the right ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="355"/>
        <source>Show the unmasked bone conduction thresholds that you&apos;ve estimated for the left ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="362"/>
        <source>Show the masked air conduction thresholds that you&apos;ve estimated for the right ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="369"/>
        <source>Show the masked air conduction thresholds that you&apos;ve estimated for the left ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="376"/>
        <source>Show the masked bone conduction thresholds that you&apos;ve estimated for the right ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="383"/>
        <source>Show the masked bone conduction thresholds that you&apos;ve estimated for the left ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="394"/>
        <source>Show the expected air conduction thresholds for the right ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="400"/>
        <source>Show 95% confidence intervals for the expected air conduction thresholds for the right ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="406"/>
        <source>Show the expected air conduction thresholds for the left ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="412"/>
        <source>Show 95% confidence intervals for the expected air conduction thresholds for the left ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="418"/>
        <source>Show the expected bone conduction thresholds for the right ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="423"/>
        <source>Show 95% confidence intervals for the expected bone conduction thresholds for the right ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="429"/>
        <source>Show the expected bone conduction thresholds for the left ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="434"/>
        <source>Show 95% confidence intervals for the expected bone conduction thresholds for the left ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="444"/>
        <source>Indicate whether the virtual listener has an earphone on the non-test ear or not. Comparing these two conditions (while no masking noise is being played) can be used to estimate the occlusion effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="455"/>
        <source>Select the status of the non-test ear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="470"/>
        <source>Select the transducer to deliver masking noise through channel 2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="480"/>
        <source>Input the level, in dB of effective masking (EM), of the masking noise.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="487"/>
        <source>Increase the level of the masking noise.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="494"/>
        <source>Decrease the level of the masking noise.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="502"/>
        <source>Turn ON the masking noise in channel 2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="518"/>
        <source>Show the filename of the case currently loaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="525"/>
        <source>Show the information available for the case currently loaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="550"/>
        <source>Show a grid on the audiogram plot.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="559"/>
        <source>This light will turn on when a stimulus is being played on channel 1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="567"/>
        <source>Play the stimulus on channel 1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="2123"/>
        <source>&lt;b&gt;audiometry_trainer&lt;/b&gt; &lt;br&gt;
                                - version: {0}; &lt;br&gt;
                                - build date: {1} &lt;br&gt;
                                &lt;p&gt; Copyright &amp;copy; 2023-2024 Samuele Carcagno. &lt;a href=&quot;mailto:sam.carcagno@gmail.com&quot;&gt;sam.carcagno@gmail.com&lt;/a&gt; 
                                All rights reserved. &lt;p&gt;
                This program is free software: you can redistribute it and/or modify
                it under the terms of the GNU General Public License as published by
                the Free Software Foundation, either version 3 of the License, or
                (at your option) any later version.
                &lt;p&gt;
                This program is distributed in the hope that it will be useful,
                but WITHOUT ANY WARRANTY; without even the implied warranty of
                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                GNU General Public License for more details.
                &lt;p&gt;
                You should have received a copy of the GNU General Public License
                along with this program.  If not, see &lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;http://www.gnu.org/licenses/&lt;/a&gt;

                &lt;p&gt; A number of icons are from Font Awesome, released under the &lt;a href=&quot;https://creativecommons.org/licenses/by/4.0/&quot;&gt;CC BY 4.0 license&lt;/a&gt;. See &lt;a href=&quot;https://gitlab.com/sam81/audiometry_trainer/-/blob/main/CREDITS.txt?ref_type=heads&quot;&gt;CREDITS.txt&lt;/a&gt; in the source distribution for details.
                &lt;p&gt;Python {2} - {3} {4} compiled against Qt {5}, and running with Qt {6} on {7}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/main_window.py" line="211"/>
        <source>Online video tutorials</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>applyChanges</name>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="478"/>
        <source>There are unsaved changes. Apply Changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="487"/>
        <source>Apply Changes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>changeFrequenciesDialog</name>
    <message>
        <location filename="../audiometry_trainer/dialog_change_frequencies.py" line="56"/>
        <source>Edit frequencies</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>generateCaseWindow</name>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="58"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="59"/>
        <source>Load parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="60"/>
        <source>Load audiogram parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="68"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="69"/>
        <source>Frequencies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="70"/>
        <source>Add/remove audiogram frequencies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="92"/>
        <source>Run simulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="96"/>
        <source>No. simulations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="140"/>
        <source>Choose file to write results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="302"/>
        <source>CSV (*.csv *CSV *Csv);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="330"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="215"/>
        <source>Invalid entry in cell[</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="268"/>
        <source>Choose parameters file to load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="268"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="330"/>
        <source>The number of simulations must be &gt; 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="63"/>
        <source>Save parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="64"/>
        <source>Save audiogram parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="104"/>
        <source>Random seed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="113"/>
        <source>Case info:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="312"/>
        <source>Case info file </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="318"/>
        <source>Choose file write case info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="318"/>
        <source>markdown (*.md);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="302"/>
        <source>Choose file to write parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="124"/>
        <source>Simulation running. This may take a while...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="200"/>
        <source>]. Psychometric function width must be positive!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="205"/>
        <source>]. Air-bone gap must be &gt;= 0!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="210"/>
        <source>]. False alarm rate must be between 0 and 1!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="215"/>
        <source>]. Lapse rate must be between 0 and 1!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/window_generate_case.py" line="146"/>
        <source> already exists, overwrite?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>preferencesDialog</name>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="70"/>
        <source>Language (requires restart):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="78"/>
        <source>Country (requires restart):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="101"/>
        <source>Marker size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="109"/>
        <source>Tracker size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="118"/>
        <source>Background Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="128"/>
        <source>Canvas Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="137"/>
        <source>DPI - Resolution:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="145"/>
        <source>Grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="273"/>
        <source>Applicatio&amp;n</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="274"/>
        <source>Plot&amp;s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="323"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="293"/>
        <source>dpi value not valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="298"/>
        <source>dpi value too small</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="305"/>
        <source>marker size value not valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="310"/>
        <source>marker size value too small</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="318"/>
        <source>tracker size value not valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="323"/>
        <source>tracker size value too small</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>setValueDialog</name>
    <message>
        <location filename="../audiometry_trainer/dialog_set_value.py" line="32"/>
        <source>Value: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audiometry_trainer/dialog_set_value.py" line="46"/>
        <source>Set Value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
