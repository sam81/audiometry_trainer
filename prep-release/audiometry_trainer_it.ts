<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
  <context>
    <name>Preferences Window</name>
    <message>
      <source>System Settings</source>
      <translation type="vanished">Setting di sistema</translation>
    </message>
  </context>
  <context>
    <name>SpreadsheetWidget</name>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="594" />
      <source>Bone R mdp</source>
      <translation>Ossea pnt. med. D</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="595" />
      <source>Bone R width</source>
      <translation>Ossea largh. D</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="596" />
      <source>ABG R</source>
      <translation>Gap Aer.-Oss. D</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="597" />
      <source>F. A. rate R</source>
      <translation>Tasso F.A. D</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="598" />
      <source>Lapse rate R</source>
      <translation>Tasso inatt. D</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="599" />
      <source>Gain R</source>
      <translation>Guadagno D</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="600" />
      <source>Bone L mdp</source>
      <translation>Ossea pnt. med. S</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="601" />
      <source>Bone L width</source>
      <translation>Ossea largh. S</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="602" />
      <source>ABG L</source>
      <translation>Gap Aer.-Oss. S</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="603" />
      <source>F. A. rate L</source>
      <translation>Tasso F.A. S</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="604" />
      <source>Lapse rate L</source>
      <translation>Tasso inatt. S</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="605" />
      <source>Gain L</source>
      <translation>Guadagno S</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="680" />
      <source>Copy</source>
      <translation>Copia</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="681" />
      <source>Cut</source>
      <translation>Taglia</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="682" />
      <source>Paste</source>
      <translation>Incolla</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="683" />
      <source>Set value</source>
      <translation>Imposta valore</translation>
    </message>
  </context>
  <context>
    <name>applicationWindow</name>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="101" />
      <source>Frequency (kHz)</source>
      <translation>Frequenza (kHz)</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="102" />
      <source>Level (dB HL)</source>
      <translation>Intensità (dB HL)</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1660" />
      <location filename="../audiometry_trainer/main_window.py" line="1541" />
      <location filename="../audiometry_trainer/main_window.py" line="1532" />
      <location filename="../audiometry_trainer/main_window.py" line="1236" />
      <location filename="../audiometry_trainer/main_window.py" line="1235" />
      <location filename="../audiometry_trainer/main_window.py" line="1234" />
      <location filename="../audiometry_trainer/main_window.py" line="1234" />
      <location filename="../audiometry_trainer/main_window.py" line="1221" />
      <location filename="../audiometry_trainer/main_window.py" line="1220" />
      <location filename="../audiometry_trainer/main_window.py" line="1219" />
      <location filename="../audiometry_trainer/main_window.py" line="1219" />
      <location filename="../audiometry_trainer/main_window.py" line="712" />
      <location filename="../audiometry_trainer/main_window.py" line="711" />
      <location filename="../audiometry_trainer/main_window.py" line="710" />
      <location filename="../audiometry_trainer/main_window.py" line="709" />
      <location filename="../audiometry_trainer/main_window.py" line="708" />
      <location filename="../audiometry_trainer/main_window.py" line="707" />
      <location filename="../audiometry_trainer/main_window.py" line="706" />
      <location filename="../audiometry_trainer/main_window.py" line="705" />
      <location filename="../audiometry_trainer/main_window.py" line="704" />
      <location filename="../audiometry_trainer/main_window.py" line="703" />
      <location filename="../audiometry_trainer/main_window.py" line="702" />
      <location filename="../audiometry_trainer/main_window.py" line="701" />
      <location filename="../audiometry_trainer/main_window.py" line="659" />
      <location filename="../audiometry_trainer/main_window.py" line="658" />
      <location filename="../audiometry_trainer/main_window.py" line="657" />
      <location filename="../audiometry_trainer/main_window.py" line="656" />
      <location filename="../audiometry_trainer/main_window.py" line="655" />
      <location filename="../audiometry_trainer/main_window.py" line="654" />
      <location filename="../audiometry_trainer/main_window.py" line="653" />
      <location filename="../audiometry_trainer/main_window.py" line="652" />
      <location filename="../audiometry_trainer/main_window.py" line="651" />
      <location filename="../audiometry_trainer/main_window.py" line="650" />
      <location filename="../audiometry_trainer/main_window.py" line="649" />
      <location filename="../audiometry_trainer/main_window.py" line="648" />
      <location filename="../audiometry_trainer/main_window.py" line="463" />
      <location filename="../audiometry_trainer/main_window.py" line="231" />
      <location filename="../audiometry_trainer/main_window.py" line="229" />
      <location filename="../audiometry_trainer/main_window.py" line="113" />
      <location filename="../audiometry_trainer/main_window.py" line="112" />
      <location filename="../audiometry_trainer/main_window.py" line="111" />
      <source>Supra-aural</source>
      <translation>Sovra-aurale</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1973" />
      <location filename="../audiometry_trainer/main_window.py" line="1873" />
      <location filename="../audiometry_trainer/main_window.py" line="1788" />
      <location filename="../audiometry_trainer/main_window.py" line="1786" />
      <location filename="../audiometry_trainer/main_window.py" line="1525" />
      <location filename="../audiometry_trainer/main_window.py" line="1254" />
      <location filename="../audiometry_trainer/main_window.py" line="245" />
      <location filename="../audiometry_trainer/main_window.py" line="122" />
      <location filename="../audiometry_trainer/main_window.py" line="115" />
      <source>Right</source>
      <translation>Destro</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="173" />
      <source>Exit</source>
      <translation>Uscita</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="175" />
      <source>Exit application</source>
      <translation>Esci dall'applicazione</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="178" />
      <source>Load case</source>
      <translation>Carica caso</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="180" />
      <source>Load case file</source>
      <translation>Carica file di caso</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="182" />
      <source>Load random case</source>
      <translation>Carica un caso estratto a sorte</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="184" />
      <source>Load random case file</source>
      <translation>Carica il file di un caso estratto a sorte</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="186" />
      <source>Generate case</source>
      <translation>Genera un caso</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="189" />
      <source>&amp;File</source>
      <translation>&amp;File</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="196" />
      <source>&amp;Edit</source>
      <translation>&amp;Modifica</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="197" />
      <source>Preferences</source>
      <translation>Preferenze</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="201" />
      <source>&amp;Help</source>
      <translation>&amp;Aiuto</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="203" />
      <source>Manual (html)</source>
      <translation>Manuale (html)</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="207" />
      <source>Manual (pdf)</source>
      <translation>Manuale (pdf)</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="211" />
      <source>Online video tutorials</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="2123" />
      <location filename="../audiometry_trainer/main_window.py" line="215" />
      <source>About audiometry_trainer</source>
      <translation>A proposito di audiometry_trainer</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="220" />
      <source>?</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="226" />
      <source>Chan. 1:</source>
      <translation>Can. 1:</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1660" />
      <location filename="../audiometry_trainer/main_window.py" line="1545" />
      <location filename="../audiometry_trainer/main_window.py" line="1536" />
      <location filename="../audiometry_trainer/main_window.py" line="1239" />
      <location filename="../audiometry_trainer/main_window.py" line="1238" />
      <location filename="../audiometry_trainer/main_window.py" line="1237" />
      <location filename="../audiometry_trainer/main_window.py" line="1237" />
      <location filename="../audiometry_trainer/main_window.py" line="1224" />
      <location filename="../audiometry_trainer/main_window.py" line="1223" />
      <location filename="../audiometry_trainer/main_window.py" line="1222" />
      <location filename="../audiometry_trainer/main_window.py" line="1222" />
      <location filename="../audiometry_trainer/main_window.py" line="726" />
      <location filename="../audiometry_trainer/main_window.py" line="725" />
      <location filename="../audiometry_trainer/main_window.py" line="724" />
      <location filename="../audiometry_trainer/main_window.py" line="723" />
      <location filename="../audiometry_trainer/main_window.py" line="722" />
      <location filename="../audiometry_trainer/main_window.py" line="721" />
      <location filename="../audiometry_trainer/main_window.py" line="720" />
      <location filename="../audiometry_trainer/main_window.py" line="719" />
      <location filename="../audiometry_trainer/main_window.py" line="718" />
      <location filename="../audiometry_trainer/main_window.py" line="717" />
      <location filename="../audiometry_trainer/main_window.py" line="716" />
      <location filename="../audiometry_trainer/main_window.py" line="715" />
      <location filename="../audiometry_trainer/main_window.py" line="672" />
      <location filename="../audiometry_trainer/main_window.py" line="671" />
      <location filename="../audiometry_trainer/main_window.py" line="670" />
      <location filename="../audiometry_trainer/main_window.py" line="669" />
      <location filename="../audiometry_trainer/main_window.py" line="668" />
      <location filename="../audiometry_trainer/main_window.py" line="667" />
      <location filename="../audiometry_trainer/main_window.py" line="666" />
      <location filename="../audiometry_trainer/main_window.py" line="665" />
      <location filename="../audiometry_trainer/main_window.py" line="664" />
      <location filename="../audiometry_trainer/main_window.py" line="663" />
      <location filename="../audiometry_trainer/main_window.py" line="662" />
      <location filename="../audiometry_trainer/main_window.py" line="661" />
      <location filename="../audiometry_trainer/main_window.py" line="463" />
      <location filename="../audiometry_trainer/main_window.py" line="231" />
      <location filename="../audiometry_trainer/main_window.py" line="229" />
      <source>Insert</source>
      <translation>Inserto</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1978" />
      <location filename="../audiometry_trainer/main_window.py" line="1878" />
      <location filename="../audiometry_trainer/main_window.py" line="1628" />
      <location filename="../audiometry_trainer/main_window.py" line="1621" />
      <location filename="../audiometry_trainer/main_window.py" line="1584" />
      <location filename="../audiometry_trainer/main_window.py" line="1547" />
      <location filename="../audiometry_trainer/main_window.py" line="1538" />
      <location filename="../audiometry_trainer/main_window.py" line="1237" />
      <location filename="../audiometry_trainer/main_window.py" line="1234" />
      <location filename="../audiometry_trainer/main_window.py" line="1195" />
      <location filename="../audiometry_trainer/main_window.py" line="699" />
      <location filename="../audiometry_trainer/main_window.py" line="698" />
      <location filename="../audiometry_trainer/main_window.py" line="697" />
      <location filename="../audiometry_trainer/main_window.py" line="696" />
      <location filename="../audiometry_trainer/main_window.py" line="695" />
      <location filename="../audiometry_trainer/main_window.py" line="694" />
      <location filename="../audiometry_trainer/main_window.py" line="693" />
      <location filename="../audiometry_trainer/main_window.py" line="692" />
      <location filename="../audiometry_trainer/main_window.py" line="691" />
      <location filename="../audiometry_trainer/main_window.py" line="690" />
      <location filename="../audiometry_trainer/main_window.py" line="689" />
      <location filename="../audiometry_trainer/main_window.py" line="688" />
      <location filename="../audiometry_trainer/main_window.py" line="646" />
      <location filename="../audiometry_trainer/main_window.py" line="645" />
      <location filename="../audiometry_trainer/main_window.py" line="644" />
      <location filename="../audiometry_trainer/main_window.py" line="643" />
      <location filename="../audiometry_trainer/main_window.py" line="642" />
      <location filename="../audiometry_trainer/main_window.py" line="641" />
      <location filename="../audiometry_trainer/main_window.py" line="640" />
      <location filename="../audiometry_trainer/main_window.py" line="639" />
      <location filename="../audiometry_trainer/main_window.py" line="638" />
      <location filename="../audiometry_trainer/main_window.py" line="637" />
      <location filename="../audiometry_trainer/main_window.py" line="636" />
      <location filename="../audiometry_trainer/main_window.py" line="635" />
      <location filename="../audiometry_trainer/main_window.py" line="231" />
      <location filename="../audiometry_trainer/main_window.py" line="229" />
      <source>Bone</source>
      <translation>Ossea</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1685" />
      <location filename="../audiometry_trainer/main_window.py" line="1582" />
      <location filename="../audiometry_trainer/main_window.py" line="1578" />
      <location filename="../audiometry_trainer/main_window.py" line="1188" />
      <location filename="../audiometry_trainer/main_window.py" line="739" />
      <location filename="../audiometry_trainer/main_window.py" line="738" />
      <location filename="../audiometry_trainer/main_window.py" line="737" />
      <location filename="../audiometry_trainer/main_window.py" line="736" />
      <location filename="../audiometry_trainer/main_window.py" line="735" />
      <location filename="../audiometry_trainer/main_window.py" line="734" />
      <location filename="../audiometry_trainer/main_window.py" line="733" />
      <location filename="../audiometry_trainer/main_window.py" line="732" />
      <location filename="../audiometry_trainer/main_window.py" line="731" />
      <location filename="../audiometry_trainer/main_window.py" line="730" />
      <location filename="../audiometry_trainer/main_window.py" line="729" />
      <location filename="../audiometry_trainer/main_window.py" line="728" />
      <location filename="../audiometry_trainer/main_window.py" line="685" />
      <location filename="../audiometry_trainer/main_window.py" line="684" />
      <location filename="../audiometry_trainer/main_window.py" line="683" />
      <location filename="../audiometry_trainer/main_window.py" line="682" />
      <location filename="../audiometry_trainer/main_window.py" line="681" />
      <location filename="../audiometry_trainer/main_window.py" line="680" />
      <location filename="../audiometry_trainer/main_window.py" line="679" />
      <location filename="../audiometry_trainer/main_window.py" line="678" />
      <location filename="../audiometry_trainer/main_window.py" line="677" />
      <location filename="../audiometry_trainer/main_window.py" line="676" />
      <location filename="../audiometry_trainer/main_window.py" line="675" />
      <location filename="../audiometry_trainer/main_window.py" line="674" />
      <location filename="../audiometry_trainer/main_window.py" line="229" />
      <source>Sound field</source>
      <translation>Campo sonoro</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="238" />
      <location filename="../audiometry_trainer/main_window.py" line="237" />
      <source>Select the transducer for the test ear.</source>
      <translation>Seleziona il trasduttore per l'orecchio test</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="243" />
      <source>Test ear:</source>
      <translation>Orecchio test:</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1792" />
      <location filename="../audiometry_trainer/main_window.py" line="1790" />
      <location filename="../audiometry_trainer/main_window.py" line="1257" />
      <location filename="../audiometry_trainer/main_window.py" line="245" />
      <source>Left</source>
      <translation>Sinistro</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="249" />
      <location filename="../audiometry_trainer/main_window.py" line="248" />
      <source>Select the test ear (right or left).</source>
      <translation>Selezione l'orecchio test (destro o sinistro)</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="253" />
      <source>Test ear status:</source>
      <translation>Stato dell'orecchio test:</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1754" />
      <location filename="../audiometry_trainer/main_window.py" line="1743" />
      <location filename="../audiometry_trainer/main_window.py" line="1726" />
      <location filename="../audiometry_trainer/main_window.py" line="1278" />
      <location filename="../audiometry_trainer/main_window.py" line="452" />
      <location filename="../audiometry_trainer/main_window.py" line="255" />
      <source>Unaided</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1756" />
      <location filename="../audiometry_trainer/main_window.py" line="1747" />
      <location filename="../audiometry_trainer/main_window.py" line="1275" />
      <location filename="../audiometry_trainer/main_window.py" line="255" />
      <source>Aided</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="258" />
      <location filename="../audiometry_trainer/main_window.py" line="257" />
      <source>Set the status (aided or unaided) of the test ear.</source>
      <translation>Imposta lo stato (con o senza protesi) dell'orecchio test</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="264" />
      <source>Test ear coupling:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1730" />
      <location filename="../audiometry_trainer/main_window.py" line="1705" />
      <location filename="../audiometry_trainer/main_window.py" line="452" />
      <location filename="../audiometry_trainer/main_window.py" line="266" />
      <source>Dome (open)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1732" />
      <location filename="../audiometry_trainer/main_window.py" line="1708" />
      <location filename="../audiometry_trainer/main_window.py" line="452" />
      <location filename="../audiometry_trainer/main_window.py" line="266" />
      <source>Dome (tulip)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1734" />
      <location filename="../audiometry_trainer/main_window.py" line="1711" />
      <location filename="../audiometry_trainer/main_window.py" line="452" />
      <location filename="../audiometry_trainer/main_window.py" line="266" />
      <source>Dome (single vent)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1736" />
      <location filename="../audiometry_trainer/main_window.py" line="1714" />
      <location filename="../audiometry_trainer/main_window.py" line="452" />
      <location filename="../audiometry_trainer/main_window.py" line="266" />
      <source>Dome (double vent)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1738" />
      <location filename="../audiometry_trainer/main_window.py" line="1717" />
      <location filename="../audiometry_trainer/main_window.py" line="452" />
      <location filename="../audiometry_trainer/main_window.py" line="266" />
      <source>Double dome (power)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1740" />
      <location filename="../audiometry_trainer/main_window.py" line="1720" />
      <location filename="../audiometry_trainer/main_window.py" line="452" />
      <location filename="../audiometry_trainer/main_window.py" line="266" />
      <source>Earmold</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="269" />
      <location filename="../audiometry_trainer/main_window.py" line="268" />
      <source>Select coupling option for the test ear.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="276" />
      <source>Show response ear</source>
      <translation>Mostra l'orecchio che risponde</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="280" />
      <location filename="../audiometry_trainer/main_window.py" line="279" />
      <source>Response light will turn red/blue if the right/left ear is responding. White if both ears respond.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="282" />
      <source>Show response counts</source>
      <translation>Mostra la conta delle risposte</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="286" />
      <location filename="../audiometry_trainer/main_window.py" line="285" />
      <source>Show the counts of responses on ascending level trials.</source>
      <translation>Mostra la conta delle risposte per i trial d'intensità ascendente</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="290" />
      <source>Auto. thresh. search</source>
      <translation>Ric. soglia auto</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="293" />
      <location filename="../audiometry_trainer/main_window.py" line="292" />
      <source>Perform an automatic threshold search using the Hughson-Westlake procedure.</source>
      <translation>Esegui una ricerca della soglia automatica utilizzando la procedure di Hughson-Westlake</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="296" />
      <source>Response light</source>
      <translation>Spia di risposta</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="303" />
      <location filename="../audiometry_trainer/main_window.py" line="302" />
      <source>The light will turn on if the virtual listener responds to the stimulus.</source>
      <translation>La spia si accenderà se l'ascoltatore virtuale risponde allo stimolo</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="306" />
      <source>Mark threshold</source>
      <translation>Marca la soglia</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="308" />
      <location filename="../audiometry_trainer/main_window.py" line="307" />
      <source>Mark the threshold at the current level.</source>
      <translation>Marca la soglia al livello corrente</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="312" />
      <source>No response</source>
      <translation>No risposta</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="314" />
      <location filename="../audiometry_trainer/main_window.py" line="313" />
      <source>Mark in the audiogram that no response was obtained at the highest stimulus level.</source>
      <translation>Marca sull'audiogramma che nessuna risposta è stata ottenuta alla più alta intensità dello stimolo.</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="317" />
      <source>Masking dilemma</source>
      <translation>Dilemma di mascheramento</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="319" />
      <location filename="../audiometry_trainer/main_window.py" line="318" />
      <source>Mark in the audiogram that the current threshold cannot be established because of a masking dilemma.</source>
      <translation>Marca sull'audiogramma che la soglia attuale non può essere stabilita a causa di un dilemma di mascheramento</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="323" />
      <source>Show estimated thresholds:</source>
      <translation>Mostra le soglie stimate:</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="328" />
      <source>Thresh. unm. air R</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="332" />
      <location filename="../audiometry_trainer/main_window.py" line="331" />
      <source>Show the unmasked air conduction thresholds that you've estimated for the right ear.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="337" />
      <source>Thresh. unm. air L</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="341" />
      <location filename="../audiometry_trainer/main_window.py" line="340" />
      <source>Show the unmasked air conduction thresholds that you've estimated for the left ear.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="344" />
      <source>Thresh. unm. bone R</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="348" />
      <location filename="../audiometry_trainer/main_window.py" line="347" />
      <source>Show the unmasked bone conduction thresholds that you've estimated for the right ear.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="351" />
      <source>Thresh. unm. bone L</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="355" />
      <location filename="../audiometry_trainer/main_window.py" line="354" />
      <source>Show the unmasked bone conduction thresholds that you've estimated for the left ear.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="358" />
      <source>Thresh. msk. air R</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="362" />
      <location filename="../audiometry_trainer/main_window.py" line="361" />
      <source>Show the masked air conduction thresholds that you've estimated for the right ear.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="365" />
      <source>Thresh. msk. air L</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="369" />
      <location filename="../audiometry_trainer/main_window.py" line="368" />
      <source>Show the masked air conduction thresholds that you've estimated for the left ear.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="372" />
      <source>Thresh. msk. bone R</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="376" />
      <location filename="../audiometry_trainer/main_window.py" line="375" />
      <source>Show the masked bone conduction thresholds that you've estimated for the right ear.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="379" />
      <source>Thresh. msk. bone L</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="383" />
      <location filename="../audiometry_trainer/main_window.py" line="382" />
      <source>Show the masked bone conduction thresholds that you've estimated for the left ear.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="387" />
      <source>Show actual thresholds:</source>
      <translation>Mostra le soglie reali:</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="391" />
      <source>Thresh. air R</source>
      <translation>Soglia aera D</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="394" />
      <location filename="../audiometry_trainer/main_window.py" line="393" />
      <source>Show the expected air conduction thresholds for the right ear.</source>
      <translation>Mostra le soglie per conduzione aerea attese per l'orecchio destro</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="431" />
      <location filename="../audiometry_trainer/main_window.py" line="420" />
      <location filename="../audiometry_trainer/main_window.py" line="409" />
      <location filename="../audiometry_trainer/main_window.py" line="397" />
      <source>CI</source>
      <translation>IC</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="400" />
      <location filename="../audiometry_trainer/main_window.py" line="399" />
      <source>Show 95% confidence intervals for the expected air conduction thresholds for the right ear.</source>
      <translation>Mostra gli intervalli di confidenza al 95% per le soglie aeree attese per l'orecchio destro</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="403" />
      <source>Thresh. air L</source>
      <translation>Soglia aerea S</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="406" />
      <location filename="../audiometry_trainer/main_window.py" line="405" />
      <source>Show the expected air conduction thresholds for the left ear.</source>
      <translation>Mostra le soglie aeree attese per l'orecchio sinistro</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="412" />
      <location filename="../audiometry_trainer/main_window.py" line="411" />
      <source>Show 95% confidence intervals for the expected air conduction thresholds for the left ear.</source>
      <translation>Mostra gli intervalli di confidenza al 95% per le soglie aeree attese per l'orecchio sinistro</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="415" />
      <source>Thresh. bone R</source>
      <translation>Soglia ossea D</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="418" />
      <location filename="../audiometry_trainer/main_window.py" line="417" />
      <source>Show the expected bone conduction thresholds for the right ear.</source>
      <translation>Mostra le soglie osee attese per l'orecchio destro</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="423" />
      <location filename="../audiometry_trainer/main_window.py" line="422" />
      <source>Show 95% confidence intervals for the expected bone conduction thresholds for the right ear.</source>
      <translation>Mostra gli intervalli di confidenza al 95% per le soglie osee attese per l'orecchio destro</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="426" />
      <source>Thresh. bone L</source>
      <translation>Soglia ossea S</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="429" />
      <location filename="../audiometry_trainer/main_window.py" line="428" />
      <source>Show the expected bone conduction thresholds for the left ear.</source>
      <translation>Mostra le soglie osee attese per l'orecchio sinistro</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="434" />
      <location filename="../audiometry_trainer/main_window.py" line="433" />
      <source>Show 95% confidence intervals for the expected bone conduction thresholds for the left ear.</source>
      <translation>Mostra gli intervalli di confidenza al 95% per le soglie osee attese per l'orecchio sinistro</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="450" />
      <location filename="../audiometry_trainer/main_window.py" line="439" />
      <source>Non-test ear status:</source>
      <translation type="unfinished">Stato dell'orecchio non testato:</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1647" />
      <location filename="../audiometry_trainer/main_window.py" line="960" />
      <location filename="../audiometry_trainer/main_window.py" line="441" />
      <source>Uncovered</source>
      <translation>Scoperto</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1692" />
      <location filename="../audiometry_trainer/main_window.py" line="1687" />
      <location filename="../audiometry_trainer/main_window.py" line="1673" />
      <location filename="../audiometry_trainer/main_window.py" line="1662" />
      <location filename="../audiometry_trainer/main_window.py" line="1652" />
      <location filename="../audiometry_trainer/main_window.py" line="1642" />
      <location filename="../audiometry_trainer/main_window.py" line="1630" />
      <location filename="../audiometry_trainer/main_window.py" line="1619" />
      <location filename="../audiometry_trainer/main_window.py" line="955" />
      <location filename="../audiometry_trainer/main_window.py" line="441" />
      <source>Earphone on</source>
      <translation>Auricolare sull'orecchio</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="444" />
      <location filename="../audiometry_trainer/main_window.py" line="443" />
      <source>Indicate whether the virtual listener has an earphone on the non-test ear or not. Comparing these two conditions (while no masking noise is being played) can be used to estimate the occlusion effect.</source>
      <translation>Indicare se l'ascoltatore virtuale ha un auricolare sull'orecchio test o no. Una comparazione di queste due condizioni (senza riproduzione di rumore attraverso l'auricolare) può essere utile a stimare l'entità dell'effetto d'occlusione</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1723" />
      <location filename="../audiometry_trainer/main_window.py" line="452" />
      <source>Chan. 2</source>
      <translation>Can. 2</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1697" />
      <location filename="../audiometry_trainer/main_window.py" line="452" />
      <source>Earplug</source>
      <translation>Tappo</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="455" />
      <location filename="../audiometry_trainer/main_window.py" line="454" />
      <source>Select the status of the non-test ear.</source>
      <translation>Seleziona lo stato dell'orecchio non testato</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="461" />
      <source>Chan. 2:</source>
      <translation>Can. 2:</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="470" />
      <location filename="../audiometry_trainer/main_window.py" line="469" />
      <source>Select the transducer to deliver masking noise through channel 2.</source>
      <translation>Seleziona il trasduttore per inviare il rumore attraverso il canale 2</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="474" />
      <source>Chan. 2 level</source>
      <translation>Intensità can. 2</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="480" />
      <location filename="../audiometry_trainer/main_window.py" line="479" />
      <source>Input the level, in dB of effective masking (EM), of the masking noise.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="487" />
      <location filename="../audiometry_trainer/main_window.py" line="486" />
      <source>Increase the level of the masking noise.</source>
      <translation>Aumenta l'intensità del rumore mascherante</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="491" />
      <source>-</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="494" />
      <location filename="../audiometry_trainer/main_window.py" line="493" />
      <source>Decrease the level of the masking noise.</source>
      <translation>Diminuisci l'intensità del rumore mascherante</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="499" />
      <source>Chan. 2 ON</source>
      <translation>Can. 2 ON</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="502" />
      <location filename="../audiometry_trainer/main_window.py" line="501" />
      <source>Turn ON the masking noise in channel 2.</source>
      <translation>Avvia il rumore mascherante sul canale 2</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="972" />
      <location filename="../audiometry_trainer/main_window.py" line="506" />
      <source>Lock channels</source>
      <translation>Aggancia i canali</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="510" />
      <location filename="../audiometry_trainer/main_window.py" line="509" />
      <source>Lock the channels so that changing the stimulus level automatically changes the masking noise level by the same amount.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="514" />
      <source>Show case filename</source>
      <translation>Nome del file di caso</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="518" />
      <location filename="../audiometry_trainer/main_window.py" line="517" />
      <source>Show the filename of the case currently loaded.</source>
      <translation>Mostra il nome del file del caso attualmente caricato</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="521" />
      <source>Show case info</source>
      <translation>Informazione del caso</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="525" />
      <location filename="../audiometry_trainer/main_window.py" line="524" />
      <source>Show the information available for the case currently loaded.</source>
      <translation>Mostra l'informazione disponibile sul caso attualmente caricato</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="529" />
      <source>Case file: </source>
      <translation>File del caso:</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="546" />
      <source>Grid</source>
      <translation>Griglia</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="550" />
      <location filename="../audiometry_trainer/main_window.py" line="549" />
      <source>Show a grid on the audiogram plot.</source>
      <translation>Mostra una griglia sull'audiogramma</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="559" />
      <location filename="../audiometry_trainer/main_window.py" line="558" />
      <source>This light will turn on when a stimulus is being played on channel 1.</source>
      <translation>Questa spia si accende quando uno stimolo è in corso di riproduzione sul canale 1</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="560" />
      <source>Stimulus light</source>
      <translation>Spia dello stimolo</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="563" />
      <source>Play stimulus</source>
      <translation>Riproduci lo stimolo</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="567" />
      <location filename="../audiometry_trainer/main_window.py" line="566" />
      <source>Play the stimulus on channel 1.</source>
      <translation>Riproduci lo stimolo sul canale 1</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="885" />
      <source>No case info available</source>
      <translation>No informazione del caso disponibile</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="888" />
      <source>Choose case file to load</source>
      <translation>Scegli il file di caso da caricare</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="888" />
      <source>CSV files (*.csv *CSV *Csv);;All Files (*)</source>
      <translation />
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="968" />
      <source>Unlock channels</source>
      <translation>Sgancia i canali</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1443" />
      <location filename="../audiometry_trainer/main_window.py" line="1394" />
      <location filename="../audiometry_trainer/main_window.py" line="1388" />
      <location filename="../audiometry_trainer/main_window.py" line="1377" />
      <location filename="../audiometry_trainer/main_window.py" line="1371" />
      <location filename="../audiometry_trainer/main_window.py" line="1329" />
      <location filename="../audiometry_trainer/main_window.py" line="1315" />
      <location filename="../audiometry_trainer/main_window.py" line="1301" />
      <location filename="../audiometry_trainer/main_window.py" line="1294" />
      <source>Warning</source>
      <translation>Avvertimento</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1302" />
      <location filename="../audiometry_trainer/main_window.py" line="1295" />
      <source>Requested channel 2 value out of limits for the current transducers</source>
      <translation>Il valore richiesto per il canale 2 va oltre i limiti del trasduttore in uso</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1316" />
      <source>Channel 2 has reached its maximum level for the current transducers</source>
      <translation>Il canale 2 ha raggiunto il livello di intensità massima per il trasduttore in uso</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1330" />
      <source>Channel 2 has reached its minimum level for the current transducers</source>
      <translation>Il canale 2 ha raggiunto il livello minimo per il trasduttore in uso</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1372" />
      <source>Channel 1 has reached the minimum level for the current transducers</source>
      <translation>Il canale 1 ha raggiunto il livello minimo per il trasduttore in uso</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1378" />
      <source>Channel 2 is locked to channel 1 and has reached its minimum level for the current transducers</source>
      <translation>Il canale 2 è agganciato al canale 1 ed ha raggiunto il livello minimo per il trasduttore in uso</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1389" />
      <source>Channel 1 has reached the maximum level for the current transducers</source>
      <translation>Il canale 1 ha raggiunto il livello massimo per il trasduttore in uso</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1395" />
      <source>Channel 2 is locked to channel 1 and has reached its maximum level for the current transducers</source>
      <translation>Il canale 2 è agganciato al canale 1 ed ha raggiunto il livello massimo per il trasduttore in uso</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1444" />
      <source>Automatic threshold search does not work when channel 2 is ON</source>
      <translation>La ricerca automatica della soglia non funziona quando il canale 2 è attivato</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="1660" />
      <location filename="../audiometry_trainer/main_window.py" line="1543" />
      <location filename="../audiometry_trainer/main_window.py" line="1534" />
      <source>Circum-aural</source>
      <translation>Circumaurale</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/main_window.py" line="2124" />
      <source>&lt;b&gt;audiometry_trainer&lt;/b&gt; &lt;br&gt;
                                - version: {0}; &lt;br&gt;
                                - build date: {1} &lt;br&gt;
                                &lt;p&gt; Copyright &amp;copy; 2023-2024 Samuele Carcagno. &lt;a href="mailto:sam.carcagno@gmail.com"&gt;sam.carcagno@gmail.com&lt;/a&gt; 
                                All rights reserved. &lt;p&gt;
                This program is free software: you can redistribute it and/or modify
                it under the terms of the GNU General Public License as published by
                the Free Software Foundation, either version 3 of the License, or
                (at your option) any later version.
                &lt;p&gt;
                This program is distributed in the hope that it will be useful,
                but WITHOUT ANY WARRANTY; without even the implied warranty of
                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                GNU General Public License for more details.
                &lt;p&gt;
                You should have received a copy of the GNU General Public License
                along with this program.  If not, see &lt;a href="http://www.gnu.org/licenses/"&gt;http://www.gnu.org/licenses/&lt;/a&gt;

                &lt;p&gt; A number of icons are from Font Awesome, released under the &lt;a href="https://creativecommons.org/licenses/by/4.0/"&gt;CC BY 4.0 license&lt;/a&gt;. See &lt;a href="https://gitlab.com/sam81/audiometry_trainer/-/blob/main/CREDITS.txt?ref_type=heads"&gt;CREDITS.txt&lt;/a&gt; in the source distribution for details.
                &lt;p&gt;Python {2} - {3} {4} compiled against Qt {5}, and running with Qt {6} and matplotlib {7} on {8}</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>applyChanges</name>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="478" />
      <source>There are unsaved changes. Apply Changes?</source>
      <translation>Modifiche non salvate. Applicare le modifiche?</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="487" />
      <source>Apply Changes</source>
      <translation>Applica le modifiche</translation>
    </message>
  </context>
  <context>
    <name>changeFrequenciesDialog</name>
    <message>
      <location filename="../audiometry_trainer/dialog_change_frequencies.py" line="56" />
      <source>Edit frequencies</source>
      <translation>Modifica le frequenze</translation>
    </message>
  </context>
  <context>
    <name>generateCaseWindow</name>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="58" />
      <source>&amp;File</source>
      <translation type="unfinished">&amp;File</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="59" />
      <source>Load parameters</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="60" />
      <source>Load audiogram parameters</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="63" />
      <source>Save parameters</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="64" />
      <source>Save audiogram parameters</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="68" />
      <source>&amp;Edit</source>
      <translation type="unfinished">&amp;Modifica</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="69" />
      <source>Frequencies</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="70" />
      <source>Add/remove audiogram frequencies</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="92" />
      <source>Run simulation</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="96" />
      <source>No. simulations</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="104" />
      <source>Random seed</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="113" />
      <source>Case info:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="124" />
      <source>Simulation running. This may take a while...</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="140" />
      <source>Choose file to write results</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="302" />
      <location filename="../audiometry_trainer/window_generate_case.py" line="140" />
      <source>CSV (*.csv *CSV *Csv);;All Files (*)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="330" />
      <location filename="../audiometry_trainer/window_generate_case.py" line="312" />
      <location filename="../audiometry_trainer/window_generate_case.py" line="215" />
      <location filename="../audiometry_trainer/window_generate_case.py" line="210" />
      <location filename="../audiometry_trainer/window_generate_case.py" line="205" />
      <location filename="../audiometry_trainer/window_generate_case.py" line="200" />
      <location filename="../audiometry_trainer/window_generate_case.py" line="196" />
      <location filename="../audiometry_trainer/window_generate_case.py" line="188" />
      <location filename="../audiometry_trainer/window_generate_case.py" line="146" />
      <source>Warning</source>
      <translation type="unfinished">Avvertimento</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="147" />
      <source>Case info file </source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="147" />
      <source> already exists, overwrite?</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="318" />
      <location filename="../audiometry_trainer/window_generate_case.py" line="152" />
      <source>Choose file write case info</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="318" />
      <location filename="../audiometry_trainer/window_generate_case.py" line="152" />
      <source>markdown (*.md);;All Files (*)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="215" />
      <location filename="../audiometry_trainer/window_generate_case.py" line="210" />
      <location filename="../audiometry_trainer/window_generate_case.py" line="205" />
      <location filename="../audiometry_trainer/window_generate_case.py" line="200" />
      <location filename="../audiometry_trainer/window_generate_case.py" line="196" />
      <source>Invalid entry in cell[</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="200" />
      <source>]. Psychometric function width must be positive!</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="205" />
      <source>]. Air-bone gap must be &gt;= 0!</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="210" />
      <source>]. False alarm rate must be between 0 and 1!</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="215" />
      <source>]. Lapse rate must be between 0 and 1!</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="268" />
      <source>Choose parameters file to load</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="268" />
      <source>All Files (*)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="302" />
      <source>Choose file to write parameters</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/window_generate_case.py" line="331" />
      <source>The number of simulations must be &gt; 0</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>preferencesDialog</name>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="70" />
      <source>Language (requires restart):</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="78" />
      <source>Country (requires restart):</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="101" />
      <source>Marker size:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="109" />
      <source>Tracker size:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="118" />
      <source>Background Color</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="128" />
      <source>Canvas Color</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="137" />
      <source>DPI - Resolution:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="145" />
      <source>Grid</source>
      <translation type="unfinished">Griglia</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="273" />
      <source>Applicatio&amp;n</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="274" />
      <source>Plot&amp;s</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="323" />
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="318" />
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="310" />
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="305" />
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="298" />
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="293" />
      <source>Warning</source>
      <translation type="unfinished">Avvertimento</translation>
    </message>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="293" />
      <source>dpi value not valid</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="298" />
      <source>dpi value too small</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="305" />
      <source>marker size value not valid</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="310" />
      <source>marker size value too small</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="318" />
      <source>tracker size value not valid</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/dialog_edit_preferences.py" line="323" />
      <source>tracker size value too small</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>setValueDialog</name>
    <message>
      <location filename="../audiometry_trainer/dialog_set_value.py" line="32" />
      <source>Value: </source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../audiometry_trainer/dialog_set_value.py" line="46" />
      <source>Set Value</source>
      <translation type="unfinished" />
    </message>
  </context>
</TS>
