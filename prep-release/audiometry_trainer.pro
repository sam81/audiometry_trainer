SOURCES	     += ../audiometry_trainer/__main__.py
SOURCES	     += ../audiometry_trainer/dialog_change_frequencies.py
SOURCES	     += ../audiometry_trainer/dialog_edit_preferences.py
SOURCES	     += ../audiometry_trainer/dialog_set_value.py
SOURCES	     += ../audiometry_trainer/global_parameters.py
SOURCES	     += ../audiometry_trainer/main_window.py
SOURCES	     += ../audiometry_trainer/window_generate_case.py
TRANSLATIONS += audiometry_trainer_it.ts audiometry_trainer_fr.ts audiometry_trainer_es.ts audiometry_trainer_el.ts audiometry_trainer_en_US.ts audiometry_trainer_en_GB.ts
