The icons arrow-down-solid, arrow-up-solid, book-solid, book-bookmark-solid, circle-info-solid, dice-solid, film-solid, floppy-disk-solid, folder-open-regular, gears-solid, headphones-solid, lock-open-solid, lock-solid, mask-solid, person-running-solid, question-solid, right-from-bracket-solid, robot-solid, skull-solid, slider-solid are from the Font Awesome project, released under the CC BY 4.0 license https://creativecommons.org/licenses/by/4.0/

The icon mask-solid-off is derived from the icon mask-solid of the Font Awesome project, released under the CC BY 4.0 license https://creativecommons.org/licenses/by/4.0/


