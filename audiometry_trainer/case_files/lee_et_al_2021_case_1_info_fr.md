__id__: lee_et_al_2021_case_1

__HL__: conductive, otosclerosis, asymmetric

__Based on__: Case 1 of Lee, H. N., Jeon, H. J., & Seo, Y. J. (2021). Familial Otosclerosis Associated with Osteogenesis Imperfecta: A Case Report. Journal of Audiology and Otology, 25(4), 230–234. https://doi.org/10.7874/jao.2021.00122

