__id__: manzari_2010

__HL__: mixed, semicircular canal dehiscence

__Based on__: case of Manzari, L. (2010). Multiple dehiscences of bony labyrinthine capsule. A rare case report and review of the literature. Acta Otorhinolaryngologica Italica: Organo Ufficiale Della Societa Italiana Di Otorinolaringologia E Chirurgia Cervico-Facciale, 30(6), 317–320.


