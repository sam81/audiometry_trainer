_id_: danesh_et_al_2018

_HL_: conductive, otosclerosis

__Based on__: Danesh, A. A., Shahnaz, N., & Hall, J. W. (2018). The Audiology of Otosclerosis. Otolaryngologic Clinics of North America, 51(2), 327–342. https://doi.org/10.1016/j.otc.2017.11.007
