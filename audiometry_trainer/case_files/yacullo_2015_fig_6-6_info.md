__id__: yacullo_2015_fig_6-6

__HL__: mixed, asymmetric

__Based on__: Figure 6.6 (page 87) of Yacullo, W. (2015). Clinical masking. In J. Katz (Ed.), Handbook of clinical audiology (7th ed., pp. 77–111). Wolters Kluwer Health.

