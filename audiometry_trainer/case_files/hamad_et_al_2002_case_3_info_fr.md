_id_: hamad_et_al_2002

_Perte_: mixte, asymétrique

__Basé sur__: Al Muhaimeed H, El Sayed Y, Rabah A, Al-Essa A. Conductive hearing loss: investigation of possible inner ear origin in three cases studies. J Laryngol Otol. 2002 Nov;116(11):942-5. doi: 10.1258/00222150260369507
