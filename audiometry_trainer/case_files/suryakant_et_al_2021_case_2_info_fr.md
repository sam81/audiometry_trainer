__id__: suryakant_et_al_2021_case_2

__Perte__: de transmission, asymétrique

__Basé sur__: cas #2 de Yadav, Suryakant; Kasera, Shejal; Prabhu, Prashanth. Reverse Masking in Clinical Audiology: An Enigma. Indian Journal of Otology 27(2):p 106-108, Apr–Jun 2021. | DOI: 10.4103/indianjotol.indianjotol_91_21 
