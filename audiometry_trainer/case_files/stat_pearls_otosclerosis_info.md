__id__: stat_pearls_case_4

__HL__: mixed, otosclerosis

__Based on__: Mixed hearing loss example from Salmon MK, Brant J, Hohman MH, et al. Audiogram Interpretation. [Updated 2023 Mar 1]. In: StatPearls [Internet]. Treasure Island (FL): StatPearls Publishing; 2024 Jan-. [Figure, Audiogram showing bilateral mixed hearing...] Available from: https://www.ncbi.nlm.nih.gov/books/NBK578179/figure/article-142867.image.f5/
