__id__: yacullo_2015_fig_6-6

__Perte__: mixte, asymétrique

__Basé sur__: Figure 6.6 (page 87) de Yacullo, W. (2015). Clinical masking. In J. Katz (Ed.), Handbook of clinical audiology (7th ed., pp. 77–111). Wolters Kluwer Health.

