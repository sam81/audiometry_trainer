__id__: valente_and_valente_2020_case_2

__Perte__: neurosensorielle, asymétrique

__Basé sur__: cas #2 de Valente, M., & Valente, M. (Eds.). (2020). Adult audiology casebook (Second edition). Thieme.
