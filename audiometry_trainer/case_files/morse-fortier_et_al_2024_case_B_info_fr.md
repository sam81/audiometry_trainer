__id__: morse-fortier_et_al_2024_case_B

__Perte__: neurosensorielle

__BasP sur__: Cas B de Morse-Fortier, C., Doney, E., Fallon, K., Remenschneider, A. (2024) Audiometric Evaluation and Diagnosis of Conductive Hearing Loss, Operative Techniques in Otolaryngology - Head and Neck Surgery,
doi: https://doi.org/10.1016/j.otot.2024.01.002

