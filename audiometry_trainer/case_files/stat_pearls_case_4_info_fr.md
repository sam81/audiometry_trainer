__id__: stat_pearls_case_4

__HL__: neurosensorielle, asymétrique

__Basé sur__: Cas #4 de Perkins CJ, Mitchell S. Audiology Clinical Masking. [Updated 2022 Mar 31]. In: StatPearls [Internet]. Treasure Island (FL): StatPearls Publishing; 2024 Jan-. Available from: https://www.ncbi.nlm.nih.gov/books/NBK580541/
