_id_: danesh_et_al_2018

_Perte_: de transmission, otosclérose

__Basé sur__: Danesh, A. A., Shahnaz, N., & Hall, J. W. (2018). The Audiology of Otosclerosis. Otolaryngologic Clinics of North America, 51(2), 327–342. https://doi.org/10.1016/j.otc.2017.11.007
