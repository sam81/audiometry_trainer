__id__: stat_pearls_case_3

__HL__: conductive, asymmetric

__Based on__: Case #3 of Perkins CJ, Mitchell S. Audiology Clinical Masking. [Updated 2022 Mar 31]. In: StatPearls [Internet]. Treasure Island (FL): StatPearls Publishing; 2024 Jan-. Available from: https://www.ncbi.nlm.nih.gov/books/NBK580541/
