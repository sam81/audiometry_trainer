__id__: kramer_and_brown_2019_fig_9-3C_base

__HL__: mixed, asymmetric

__Based on__: Case from Figure 9-3C of Kramer, S. J., Brown, D. K., Jerger, J., & Mueller, H. G. (2019). Audiology: Science to practice (Third edition). Plural Publishing.

