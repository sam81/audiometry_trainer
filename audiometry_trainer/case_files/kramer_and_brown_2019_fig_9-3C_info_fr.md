__id__: kramer_and_brown_2019_fig_9-3C_base

__Perte__: mixte, asymétrique

__Basé sur__: Cas de la Figure 9-3C de Kramer, S. J., Brown, D. K., Jerger, J., & Mueller, H. G. (2019). Audiology: Science to practice (Third edition). Plural Publishing.

