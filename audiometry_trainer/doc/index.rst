.. audiometry_trainer documentation master file, created by
   sphinx-quickstart on Thu Feb 23 16:00:48 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to audiometry_trainer's documentation!
==============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   installation
   threshold_search
   masking
   virtual_listener
   generate_case
   user_interface
   internals
   references


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
