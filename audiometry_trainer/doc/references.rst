.. raw:: latex

   \cleardoublepage
   \begingroup
   \renewcommand\chapter[1]{\endgroup}
   \phantomsection

.. _refs:

**********
References
**********

.. [Alcalá-QuintanaAndGarcía-Pérez2004] Alcalá-Quintana, R., & García-Pérez, M. A. (2004). The Role of Parametric Assumptions in Adaptive Bayesian Estimation. Psychological Methods, 9(2), 250–271. https://doi.org/10.1037/1082-989X.9.2.250
					
.. [Barr-HamiltonEtAl1969] Barr-Hamilton, R. M., Bryan, M. E., & Tempest, W. (1969). Applications of Signal Detection Theory to Audiometry. International Audiology, 8(1), 138–146. https://doi.org/10.3109/05384916909070201

.. [FagelsonAndMartin1994] Fagelson, M., & Martin, F. N. (1994). Sound pressure in the external auditory canal during bone-conduction testing. Journal of the American Academy of Audiology, 5(6), 379–383.

.. [Gelfand2016] Gelfand, S. A. (2016). Essentials of audiology (Fourth edition). Thieme.
			   			   
.. [KingdomAndPrins2016] Kingdom, F. A. A., & Prins, N. (2016). Psychophysics: A practical introduction (Second edition). Elsevier/Academic Press.

.. [KussEtAl2005] Kuss, M., Jäkel, F., & Wichmann, F. A. (2005). Bayesian inference for psychometric functions. Journal of Vision, 5(5), 8. https://doi.org/10.1167/5.5.8

.. [MarshallAndJesteadt1986] Marshall, L., & Jesteadt, W. (1986). Comparison of Pure-Tone Audibility Thresholds Obtained with Audiological and Two-Interval Forced-Choice Procedures. Journal of Speech, Language, and Hearing Research, 29(1), 82–91. https://doi.org/10.1044/jshr.2901.82

.. [MartinEtAl1974] Martin, F. N., Butler, E. C., & Burns, P. (1974). Audiometric Bing Test for Determination of Minimum Masking Levels for Bone-Conduction Tests. Journal of Speech and Hearing Disorders, 39(2), 148–152. https://doi.org/10.1044/jshd.3902.148

.. [MunroAndAgnew1999] Munro, K. J., & Agnew, N. (1999). A comparison of inter-aural attenuation with the Etymotic ER-3A insert earphone and the Telephonies TDH-39 supra-aural earphone. British Journal of Audiology, 33(4), 259–262. https://doi.org/10.3109/03005369909090106

.. [Stenfelt2012] Stenfelt, S. (2012). Transcranial Attenuation of Bone-Conducted Sound When Stimulation Is at the Mastoid and at the Bone Conduction Hearing Aid Position. Otology & Neurotology, 33(2), 105–114. https://doi.org/10.1097/MAO.0b013e31823e28ab
		       
.. [Studebaker1967] Studebaker, G. A. (1967). Clinical masking of the nontest ear. The Journal of Speech and Hearing Disorders, 32(4), 360–371. https://doi.org/10.1044/jshd.3204.360

.. [Turner2004] Turner, R. G. (2004). Masking Redux II: A Recommended Masking Protocol. Journal of the American Academy of Audiology, 15(01), 029–046. https://doi.org/10.3766/jaaa.15.1.5


.. [Yacullo1997] Yacullo, W. S. (1997). Clinical masking procedures. Needham Heights: Allyn and Bacon.
